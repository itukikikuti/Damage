#include "XLibrary11.hpp"
using namespace XLibrary11;

enum Phase
{
	Select,
	PlayerAttack,
	EnemyDamage,
	EnemyAttack,
	PlayerDamage,
	Win,
	Loose
};

int Main()
{
	Camera camera;

	Phase phase = Phase::Select;

	Sprite foreBar(L"ForeBar.png");
	Sprite backBar(L"BackBar.png");

	Text messageText(L"");
	Text selectText(L"");

	messageText.position = Float2(-100.0f, -200.0f);
	selectText.position = Float2(200.0f, -200.0f);

	Sprite playerSprite(L"mon_012.gif");
	Text playerText(L"フォーク ♀ Lv.100");
	float playerHP = 300.0f;
	float playerHPMax = 300.0f;
	float playerDamage = 0.0f;
	int playerSkill = 0;

	playerSprite.scale = Float2(2.0f, 2.0f);
	playerSprite.position = Float2(-150.0f, 0.0f);
	playerText.position = Float2(-150.0f, 150.0f);

	Sprite enemySprite(L"mon_014.gif");
	Text enemyText(L"ミノタウロス ♂ Lv.100");

	enemySprite.scale.x = -1.0f;
	enemySprite.position = Float2(100.0f, 50.0f);
	enemyText.position = Float2(100.0f, 220.0f);
	float enemyHP = 500.0f;
	float enemyHPMax = 500.0f;
	float enemyDamage = 0.0f;

	Sound bgm(L"BGM.mp3");
	bgm.SetLoop(true);
	bgm.SetVolume(0.1f);
	bgm.Play();

	Sound attackSound(L"Attack.wav");
	Sound okSound(L"OK.wav");

	while (Refresh())
	{
		camera.Update();

		switch (phase)
		{
		case Phase::Select:
			if (Input::GetKeyDown('1'))
			{
				playerSkill = 1;
				phase = Phase::PlayerAttack;
				okSound.Play();
			}
			if (Input::GetKeyDown('2'))
			{
				playerSkill = 2;
				phase = Phase::PlayerAttack;
				okSound.Play();
			}
			if (Input::GetKeyDown('3'))
			{
				playerSkill = 3;
				phase = Phase::PlayerAttack;
				okSound.Play();
			}
			if (Input::GetKeyDown('4'))
			{
				playerSkill = 4;
				phase = Phase::PlayerAttack;
				okSound.Play();
			}

			messageText.Create(L"フォークはどうする？");
			selectText.Create(L"1 ひっかく 30\n2 たいあたり 400\n3 つつく 10\n4 ずつき 40");
			selectText.Draw();
			break;
		case Phase::PlayerAttack:
			if (playerSkill == 1)
			{
				messageText.Create(L"フォークのひっかく！");
				enemyDamage = 30.0f;
			}
			if (playerSkill == 2)
			{
				messageText.Create(L"フォークのたいあたり！");
				enemyDamage = 400.0f;
			}
			if (playerSkill == 3)
			{
				messageText.Create(L"フォークのつつく！");
				enemyDamage = 10.0f;
			}
			if (playerSkill == 4)
			{
				messageText.Create(L"フォークのずつき！");
				enemyDamage = 40.0f;
			}
			if (Input::GetKeyDown(VK_RETURN))
			{
				phase = Phase::EnemyDamage;
				attackSound.Play();
			}
			break;
		case Phase::EnemyDamage:
			enemyDamage--;
			enemyHP--;

			if (enemyDamage <= 0.0f)
			{
				phase = Phase::EnemyAttack;
			}
			if (enemyHP <= 0.0f)
			{
				phase = Phase::Win;
			}
			break;
		case Phase::EnemyAttack:
			messageText.Create(L"ミノタウロスのかみつく！");
			playerDamage = 30.0f;
			if (Input::GetKeyDown(VK_RETURN))
			{
				phase = Phase::PlayerDamage;
				attackSound.Play();
			}
			break;
		case Phase::PlayerDamage:
			playerDamage--;
			playerHP--;

			if (playerDamage <= 0.0f)
			{
				phase = Phase::Select;
			}
			if (playerHP <= 0.0f)
			{
				phase = Phase::Loose;
			}
			break;
		case Phase::Win:
			messageText.Create(L"ミノタウロスをたおした！");
			break;
		case Phase::Loose:
			messageText.Create(L"フォークはたおれた…");
			break;
		}

		messageText.Draw();

		playerSprite.Draw();
		playerText.Draw();

		if (playerHP / playerHPMax <= 1.0f)
		{
			foreBar.color = Float4(0.0f, 1.0f, 0.0f, 1.0f);
		}
		if (playerHP / playerHPMax <= 0.5f)
		{
			foreBar.color = Float4(1.0f, 1.0f, 0.0f, 1.0f);
		}
		if (playerHP / playerHPMax <= 0.25f)
		{
			foreBar.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		}

		foreBar.position = Float2(-150.0f + playerHP / playerHPMax * 50.0f - 50.0f, 130.0f);
		foreBar.scale = Float2(playerHP / playerHPMax, 1.0f);
		backBar.position = Float2(-150.0f, 130.0f);

		backBar.Draw();
		foreBar.Draw();

		enemySprite.Draw();
		enemyText.Draw();

		if (enemyHP / enemyHPMax <= 1.0f)
		{
			foreBar.color = Float4(0.0f, 1.0f, 0.0f, 1.0f);
		}
		if (enemyHP / enemyHPMax <= 0.5f)
		{
			foreBar.color = Float4(1.0f, 1.0f, 0.0f, 1.0f);
		}
		if (enemyHP / enemyHPMax <= 0.25f)
		{
			foreBar.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		}

		foreBar.position = Float2(100.0f + enemyHP / enemyHPMax * 50.0f - 50.0f, 200.0f);
		foreBar.scale = Float2(enemyHP / enemyHPMax, 1.0f);
		backBar.position = Float2(100.0f, 200.0f);

		backBar.Draw();
		foreBar.Draw();
	}
}
